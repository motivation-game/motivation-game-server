#!/usr/bin/env bash

# more bash-friendly output for jq
JQ="jq --raw-output --exit-status"

configure_aws_cli(){
	aws --version
	aws configure set default.region eu-central-1
	aws configure set default.output json
}

push_ecr_image(){
	eval $(aws ecr get-login --region eu-central-1 --no-include-email)
    docker build -t motivation-game-server:latest .
	docker tag motivation-game-server:latest 636301108823.dkr.ecr.eu-central-1.amazonaws.com/motivation-game-server:latest
	docker push 636301108823.dkr.ecr.eu-central-1.amazonaws.com/motivation-game-server:latest
}

redeploy_service() {
    aws ecs update-service --cluster motivation-game-server --service motivation-game-server-service --force-new-deployment
	aws ecs wait services-stable --cluster motivation-game-server --services motivation-game-server-service
}

configure_aws_cli
push_ecr_image
redeploy_service