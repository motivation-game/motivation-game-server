import * as express from "express";
import * as socketio from "socket.io";
import * as path from "path";
import { Player } from './player';

const app = express();
app.set("port", process.env.PORT || 3000);

let http = require("http").Server(app);
let io = require("socket.io")(http);

let players: Array<Player> = new Array<Player>();
let turn: number = 0;
let round: number = 1;
let maxRounds: number = 10;

io.on("connection", (socket: any) => {
    console.log("a user connected");
    
    socket.on('new-player', (name:string) => {
      console.log('new player ' + name + ' connected.')
      let player: Player = new Player()
      player.id = socket.id
      player.name = name

      players.push(player);
      io.emit('update-players', players);
    });

    socket.on('start', () => {
      io.emit('start');
      round=1
      turn=0
      io.emit('new-turn', players[0].name);
      console.log('round: ' + round)
    });

    socket.on('play-card', (card:number, eventCard:number) => {
      console.log('card ' + card + ' played')
      socket.broadcast.emit('card-played', card, eventCard);
      nextPlayerTurn()
      console.log(turn + ' player is up')
      console.log('round: ' + round)
      if (round > maxRounds) {
        io.emit('game-end');
      } else {
        io.emit('new-turn', players[turn].name);
      }
    });

    socket.on('score', (name: string, score: number) => {
      let collectedAllScores = true;
      for (let player of players) {
        if (player.name === name) {
          player.score = score;
        }

        if (player.score === undefined) {
          collectedAllScores = false
        }
      }

      if (collectedAllScores) {
        io.emit('final-scores', players)
      }
    });

    socket.on('disconnect', () => {
      console.log("player disconnected");
      for (let i in players) {
        if (players[i].id === socket.id) {
          players.splice(+i,1)
        }
      }
      io.emit('update-players', players)
    })
});

let nextPlayerTurn = () => {
  turn++;
  if (turn >= players.length) {
    turn = 0;
    round++;
  }
}

const server = http.listen(3000, function() {
  console.log("listening on *:3000");
});